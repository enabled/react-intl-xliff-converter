var reactIntlToXLIFF = require('./app/reactIntlToXLIFF');
var XLIFFToReactIntl = require('./app/XLIFFToReactIntl');

module.exports = {
  // programmatic usage, returns result
  generateReactIntl: XLIFFToReactIntl.generateReactIntl,
  generateXLIFF: reactIntlToXLIFF.generateXLIFF,

  // script / CLI usage, load and save files
  createXLIFFFromSource: reactIntlToXLIFF.createXLIFFFromSource,
  createReactIntlFile: XLIFFToReactIntl.createReactIntlFile,
  createXLIFFFromSourceAndTarget: reactIntlToXLIFF.createXLIFFFromSourceAndTarget,
};
