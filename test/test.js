/* global describe */
/* global it */
/* global beforeEach */
var XLIFFToReactIntl = require('../app/XLIFFToReactIntl');
const assert = require('assert');

const fs = require('fs');
const xliff1 = fs.readFileSync(__dirname + '/../test-data/en-US-partial-translation.xlf', 'utf8');


describe('xliff to react intl', function() {
  it('correctly converts a partially translated XLIFF file', function() {
    const expected = {
      data: {
        "Greeting": "Hello! How are you going?"
      },
      language: 'en-US',
    };
    return XLIFFToReactIntl.generateReactIntl(xliff1)
      .then(function(result) {
        assert.deepEqual(expected, result);
      });
  })
});
