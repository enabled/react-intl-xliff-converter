var fs = require('fs');
var xml2js = require('xml2js');
var isNullOrUndefined = require('./utils').isNullOrUndefined;
var promisify = require('promisify-node');
var Parser = promisify(xml2js.Parser);

function readFile(filename) {
  return fs.readFileSync(filename, 'utf8');
}

function saveReactIntlFile(reactIntlMap, filename) {
  fs.writeFileSync(filename, JSON.stringify(reactIntlMap, null, 2), 'utf8');
  console.log('saved to ' + filename);
}

/**
 * @param parent
 * @param elementName
 * @returns object
 * Gets a specified child element or property. Throws if property doesn't exist.
 */
function getChildElement(parent, elementName) {
  var element = parent[elementName];
  if (isNullOrUndefined(element)) {
    throw new Error('element "' + elementName + '" expected and was not found');
  }
  return element;
}

function getAttribute(element, attributeName) {
  return getChildElement(element, '$')[attributeName];
}

function getInnerText(element) {
  try {
    return getChildElement(element, '_');
  } catch(error) {
    if (error.message === 'element "_" expected and was not found') {
      return null;
    }
  }
}

/**
 * @param xliffSource file contents of source XLIFF file
 * @returns Promise<object> {
 *   data: React Intl map derived from XLIFF target data
 *   language: language from XLIFF target
 * }
 */
function generateReactIntl(xliffSource) {
    var parser = new Parser();
    return parser.parseString(xliffSource)
      .then(function(xliffData) {
        var xliff = getChildElement(xliffData, 'xliff');
        var file = getChildElement(xliff, 'file')[0];
        var fileAttributes = getChildElement(file, '$');
        var targetLanguage = getChildElement(fileAttributes, 'target-language');
        var body = getChildElement(file, 'body')[0];
        var transUnits = getChildElement(body, 'trans-unit');

        var outputMap = {};

        transUnits.forEach(function (item) {
          var key = getAttribute(item, 'id');
          var target = item.target[0];

          // target text may be contained in an mrk tag
          if (target.mrk && target.mrk[0]) {
            target = getChildElement(target, 'mrk')[0];
          }
          var message = getInnerText(target);
          //don't output untranslated messages
          if (message === null || message === '') {
            return;
          }

          outputMap[key] = message;
        });

        return {
          data: outputMap,
          language: targetLanguage,
        };
      });
}

/**
 * @param sourceFile file path of source XLIFF file
 * @param outputFilename (optional)
 * @returns Promise<object>
 */
function createReactIntlFile(sourceFile, outputFilename) {
  var fileContents = readFile(sourceFile);
  return generateReactIntl(fileContents)
    .then(function(output) {
      var outputReactIntl = output.data;
      var outputLanguage = output.language;

      saveReactIntlFile(outputReactIntl, outputFilename || (outputLanguage + '.json'));
    });
}

module.exports = {
  createReactIntlFile: createReactIntlFile,
  generateReactIntl: generateReactIntl,
};