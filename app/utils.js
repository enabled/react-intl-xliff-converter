function isNullOrUndefined(item) {
  return item === null || typeof item === 'undefined';
}

module.exports = {
  isNullOrUndefined: isNullOrUndefined,
};
