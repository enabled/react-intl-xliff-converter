var fs = require('fs');
var builder = require('xmlbuilder');
var isNullOrUndefined = require('./utils').isNullOrUndefined;

function readReactIntlFile(filename) {
  const fileContents = fs.readFileSync(filename, 'utf8');
  return JSON.parse(fileContents);
}

function saveXLIFFFile(xmlDoc, filename) {
  fs.writeFileSync(filename, xmlDoc, 'utf8');
  console.log('saved to ' + filename)
}

function createXLIFFRoot() {
  return builder.create({
    xliff: {
      '@xmlns': 'urn:oasis:names:tc:xliff:document:1.2',
      '@version': '1.2',
    }
  });
}

function createBodyElement(parentElement, sourceLanguage, targetLanguage) {
  var file = parentElement.ele('file', {
    original: 'global',
    datatype: 'plaintext',
    'source-language': sourceLanguage,
    'target-language': targetLanguage,
  });

  var body = file.ele('body');

  return body;
}

/**
 *
 * @param parentElement
 * @param messageKey id of translation message
 * @param sourceLanguage
 * @param sourceMessage message in source language
 * @param targetLanguage
 * @param targetMessage message in target language (optional)
 * @returns {Object}
 * Appends a new trans-unit element to the parentElement, containing a source and target.
 * Returns the new element.
 */
function createTransUnitElement(parentElement, messageKey, sourceLanguage, sourceMessage, targetLanguage, targetMessage) {
  if(isNullOrUndefined(parentElement)) {
    throw new Error('no parent element specified');
  }

  var transUnit = createElement('trans-unit', parentElement, {
    'id': messageKey,
  });

  var source = createElement('source', transUnit, {
    'xml:lang': sourceLanguage,
  }, sourceMessage);

  var target = createElement('target', transUnit, {
    'xml:lang': targetLanguage,
  }, targetMessage || '');

  return transUnit;

}

/**
 * @param elementName
 * @param parentElement
 * @param attributes
 * @param elementText
 *
 *  appends an element to parentElement in the form
 *   <elementName ...attributes>
 *      elementText
 *   </elementName>
 *   @returns Object the created element
 */
function createElement(elementName, parentElement, attributes, elementText) {
  //Check if parentElement is defined, throw exception
  if(isNullOrUndefined(parentElement)) {
    throw new Error('no parent element specified');
  }

  var newElement = parentElement.ele(elementName, attributes);
  if (!isNullOrUndefined((elementText))) {
    newElement.txt(elementText);
  }

  return newElement;
}

/**
 * @param sourceLanguage language of untranslated data (required)
 * @param sourceData map of message keys to messages (required)
 * @param targetLanguage language of translated data (required)
 * @param targetData map of translated or partially translated data (optional)
 * @returns string XLIFF file contents
 */
function generateXLIFF(sourceLanguage, sourceData, targetLanguage, targetData) {
  if (isNullOrUndefined(targetData)) {
    targetData = {};
  }

  var root = createXLIFFRoot();
  var body = createBodyElement(root, sourceLanguage, targetLanguage);

  for (var messageKey in sourceData) {
    var message = sourceData[messageKey];
    var translatedMessage = targetData[messageKey];

    createTransUnitElement(body, messageKey, sourceLanguage, message, targetLanguage, translatedMessage);
  }

  return root.end({pretty: true}).toString();
}

/**
 * @param sourceLanguage language of untranslated data (required)
 * @param sourceFile file path of source language file (required)
 * @param targetLanguage language of translated data (required)
 * @param targetFile file path of target file translated or partially translated data (optional)
 * @param outputFilename file path to save output file to (optional)
 * Wrapper for generateXLIFF() that loads source files, generates and saves an XLIFF file.
 * Takes two React Intl files (in two distinct languages) and create an XLIFF file containing all messages and their target language translations.
 */
function createXLIFFFromSourceAndTarget(sourceLanguage, sourceFile, targetLanguage, targetFile, outputFilename) {
  var sourceData = readReactIntlFile(sourceFile);
  var targetData = readReactIntlFile(targetFile);
  var outputFile = generateXLIFF(sourceLanguage, sourceData, targetLanguage, targetData);
  saveXLIFFFile(outputFile, outputFilename || (targetLanguage + ".xlf"));
}

/*
 Take a source React Intl file and output an XLIFF file with empty elements for the target language

 */
function createXLIFFFromSource(sourceLanguage, sourceFile, targetLanguage, outputFilename) {
  var sourceData = readReactIntlFile(sourceFile);
  var outputFile = generateXLIFF(sourceLanguage, sourceData, targetLanguage);
  return saveXLIFFFile(outputFile, outputFilename || (targetLanguage + ".xlf"));
}

module.exports = {
  createXLIFFFromSourceAndTarget: createXLIFFFromSourceAndTarget,
  createXLIFFFromSource: createXLIFFFromSource,
  generateXLIFF: generateXLIFF,
};