var reactIntlXLIFFConverter = require('../index');

var sourceLanguage = process.argv[2];
var sourceFile = process.argv[3];
var targetLanguage = process.argv[4];
var targetFile = process.argv[5];
var optionalFilename = process.argv[6];

try {
  reactIntlXLIFFConverter.createXLIFFFromSourceAndTarget(sourceLanguage, sourceFile, targetLanguage, targetFile, optionalFilename);
} catch(error) {
  console.error(error);
  throw error;
}
