var reactIntlXLIFFConverter = require('../index');
var fs = require('fs');

var input = {
  "Greeting": "G'day mate! How's it going?",
  "Farewell": "Seeya later mate."
};

var output = reactIntlXLIFFConverter.generateXLIFF('en-AU', input, 'en-US');

console.log('generateXLIFF');
console.log('input:');
console.log(input, '\n');
console.log('output:');
console.log(output, '\n');

var xliffInput = fs.readFileSync('./test-data/en-US.xlf', 'utf8');

return reactIntlXLIFFConverter.generateReactIntl(xliffInput)
  .then(function(output) {
    console.log('generateReactIntl');
    console.log('input:');
    console.log(xliffInput, '\n');
    console.log('output:');
    console.log(output, '\n');
  });
