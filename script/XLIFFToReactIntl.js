var reactIntlXLIFFConverter = require('../index');

var sourceFile = process.argv[2];
var optionalFilename = process.argv[3];

return reactIntlXLIFFConverter.createReactIntlFile(sourceFile, optionalFilename)
  .catch(function(error) {
    console.error(error);
    throw error;
  });
