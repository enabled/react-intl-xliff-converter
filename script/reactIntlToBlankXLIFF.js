var reactIntlXLIFFConverter = require('../index');

var sourceLanguage = process.argv[2];
var sourceFile = process.argv[3];
var targetLanguage = process.argv[4];
var optionalFilename = process.argv[5];

try {
  reactIntlXLIFFConverter.createXLIFFFromSource(sourceLanguage, sourceFile, targetLanguage, optionalFilename);
} catch(error) {
  console.error(error);
  throw error;
}
