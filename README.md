# React Intl XLIFF Converter
CLI tools for converting between [React Intl](https://github.com/yahoo/react-intl) json files [XLIFF](http://docs.oasis-open.org/xliff/xliff-core/xliff-core.html) XML files.

## Support
This library currently supports simple XLIFF files in the following format:
```xml
<?xml version="1.0"?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file original="global" datatype="plaintext" source-language="en-AU" target-language="en-US">
    <body>
      <trans-unit id="Greeting">
        <source xml:lang="en-AU">G'day mate! How's it going?</source>
        <target xml:lang="en-US"/>
      </trans-unit>
      <trans-unit id="...">
        ...
      </trans-unit>
      <trans-unit id="...">
        ...
      </trans-unit>
    </body>
  </file>
</xliff>
```


## Usage as Module
The following functions can be imported for use if you don't want to load or save files, or if you want to handle it yourself.

`generateXLIFF(sourceLanguage, sourceData, targetLanguage, targetData)`

>returns `string`

`generateReactIntl(xliffSource)`

>returns `Promise<{ data: object, language: string }>`

## CLI Usage
These functions can also be imported, but are designed for CLI usage:

### React Intl source language file to XLIFF
> Convert React Intl files in a source language to XLIFF files, ready for translation into a target language.

```bash
npm run react-intl-to-blank-xliff sourceLanguage sourceInputFile targetLanguage outputFile
```
* sourceLanguage: language code of the source file
* sourceInputFile: file path of the source file
* targetLanguage: language code of the target langauge
* outputFile: path of output file; optional, defaults to "\<sourceLanguage\>.json"

#### Example

Input:

en-AU.json
```json
{
  "Greeting": "G'day mate! How's it going?",
  "Farewell": "Seeya later mate."
}
```
Output:

en-US.xlf
```xml
<?xml version="1.0"?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file original="global" datatype="plaintext" source-language="en-AU" target-language="en-US">
    <body>
      <trans-unit id="Greeting">
        <source xml:lang="en-AU">G'day mate! How's it going?</source>
        <target xml:lang="en-US"/>
      </trans-unit>
      <trans-unit id="Farewell">
        <source xml:lang="en-AU">Seeya later mate.</source>
        <target xml:lang="en-US"/>
      </trans-unit>
    </body>
  </file>
</xliff>
```

### React Intl source and partial target files to XLIFF
> Convert a React Intl file in a source language and a partial React Intl file in a target language into a single XLIFF file.

```bash
npm run react-intl-to-xliff sourceLanguage sourceInputFile targetLanguage targetInputFile outputFile
```
* sourceLanguage: language code of the source file
* sourceInputFile: file path of the source file
* targetLanguage: language code of the target langauge
* targetInputFile: file path of the (possibly partial) target file
* outputFile: path of output file; optional, defaults to "\<sourceLanguage\>.json"

#### Example

Input:

en-AU.json
```json
{
  "Greeting": "G'day mate! How's it going?",
  "Farewell": "Seeya later mate."
}
```

en-US.json
```json
{
  "Greeting": "Hello! How are you going?"
}
```
Output:

en-US.xlf
```xml
<?xml version="1.0"?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file original="global" datatype="plaintext" source-language="en-AU" target-language="en-US">
    <body>
      <trans-unit id="Greeting">
        <source xml:lang="en-AU">G'day mate! How's it going?</source>
        <target xml:lang="en-US">Hello! How are you going?</target>
      </trans-unit>
      <trans-unit id="Farewell">
        <source xml:lang="en-AU">Seeya later mate.</source>
        <target xml:lang="en-US"/>
      </trans-unit>
    </body>
  </file>
</xliff>
```

### XLIFF to React Intl
> Convert translations from an XLIFF file into a single React Intl file for the target language

```bash
npm run xliff-to-react-intl sourceInputFile outputFile
```
* sourceInputFile: file path of the source file
* outputFile: path of output file; optional, defaults to "\<targetLang\>.json" where `targetLang` is the target language specified in the source file. 

Input:

en-US.xlf
```xml
<?xml version="1.0"?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file original="global" datatype="plaintext" source-language="en-AU" target-language="en-US">
    <body>
      <trans-unit id="Greeting">
        <source xml:lang="en-AU">G'day mate! How's it going?</source>
        <target xml:lang="en-US">Hello! How are you going?</target>
      </trans-unit>
      <trans-unit id="Farewell">
        <source xml:lang="en-AU">Seeya later mate.</source>
        <target xml:lang="en-US">See you later.</target>
      </trans-unit>
    </body>
  </file>
</xliff>
```

Output:

en-US.json
```json
{
  "Greeting": "Hello! How are you going?",
  "Farewell": "See you later."
}
```